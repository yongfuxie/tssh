module github.com/trzsz/trzsz-ssh

go 1.20

require (
	github.com/alessio/shellescape v1.4.2
	github.com/armon/go-socks5 v0.0.0-20160902184237-e75332964ef5
	github.com/chzyer/readline v1.5.1
	github.com/google/shlex v0.0.0-20191202100458-e7afc7fbc510
	github.com/mattn/go-isatty v0.0.20
	github.com/mitchellh/go-homedir v1.1.0
	github.com/skeema/knownhosts v1.2.1
	github.com/stretchr/testify v1.8.4
	github.com/trzsz/go-arg v1.5.2
	github.com/trzsz/iterm2 v0.1.0
	github.com/trzsz/npipe v0.1.0
	github.com/trzsz/promptui v0.10.3
	github.com/trzsz/ssh_config v1.3.3
	github.com/trzsz/trzsz-go v1.1.7-0.20231111144918-b45bed013817
	golang.org/x/crypto v0.15.0
	golang.org/x/sys v0.14.0
	golang.org/x/term v0.14.0
)

require (
	github.com/UserExistsError/conpty v0.1.1 // indirect
	github.com/akavel/rsrc v0.10.2 // indirect
	github.com/alexflint/go-scalar v1.2.0 // indirect
	github.com/andybrewer/mack v0.0.0-20220307193339-22e922cc18af // indirect
	github.com/creack/pty v1.1.20 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/dchest/jsmin v0.0.0-20220218165748-59f39799265f // indirect
	github.com/gorilla/websocket v1.5.1 // indirect
	github.com/josephspurrier/goversioninfo v1.4.0 // indirect
	github.com/klauspost/compress v1.17.2 // indirect
	github.com/ncruces/zenity v0.10.10 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/randall77/makefat v0.0.0-20210315173500-7ddd0e42c844 // indirect
	golang.org/x/image v0.14.0 // indirect
	golang.org/x/net v0.18.0 // indirect
	golang.org/x/text v0.14.0 // indirect
	google.golang.org/protobuf v1.31.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
